--index-url http://mirrors.cloud.tencent.com/pypi/simple

attrs==21.2.0; (python_version >= "2.7" and python_full_version < "3.0.0") or (python_full_version >= "3.5.0")
hyperlink==21.0.0; (python_version >= "2.6" and python_full_version < "3.0.0") or (python_full_version >= "3.4.0")
idna==3.3; python_version >= "3.5" and python_full_version < "3.0.0" or python_full_version >= "3.4.0" and python_version >= "3.5"
mutagen==1.45.1; python_version >= "3.5" and python_version < "4"
