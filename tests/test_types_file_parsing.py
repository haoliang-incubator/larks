import pytest
from larks.types.file.creator import (
    parse_album_string,
    parse_disc_string,
    parse_track_string,
)


def test_parse_album_string():

    feed = [
        (
            "ABBA - GOLD - Greatest Hits 1973-1981 (1992) [FLAC]",
            ("ABBA", "GOLD - Greatest Hits 1973-1981", 1992, ["flac"]),
        ),
        (
            "Adele - 21 Limited Edition [FLAC]",
            ("Adele", "21 Limited Edition", None, ["flac"]),
        ),
        (
            "Adele - 21 Limited Edition [FLAC,MP3]",
            ("Adele", "21 Limited Edition", None, ["flac", "mp3"]),
        ),
    ]

    for string, expected in feed:
        album = parse_album_string(string)
        assert album == expected


def test_parse_album_string_failed():
    string = "Ane Brun - Songs 2003-2013 - 2CD"

    with pytest.raises(ValueError) as exc:
        parse_album_string(string)

    assert "due to suffix" in str(exc)


def test_parse_track_string():
    feed = [
        (
            "01 - Dancing Queen",
            (1, "Dancing Queen"),
        ),
        (
            "01. Ode To My Family",
            (1, "Ode To My Family"),
        ),
        (
            "01 Ode To My Family",
            (1, "Ode To My Family"),
        ),
    ]

    for string, expected in feed:
        track = parse_track_string(string)
        assert track == expected


def test_parse_disc_string():
    feed = [
        (
            "Disc 1 - Hotel California",
            (1, "Hotel California"),
        ),
        (
            "Disc 2",
            (2, None),
        ),
        (
            "CD 3",
            (3, None),
        ),
    ]

    for path, expected in feed:
        disc = parse_disc_string(path)
        assert disc == expected
