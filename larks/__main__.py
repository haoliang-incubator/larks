#!/usr/bin/env python3

import os
from argparse import ArgumentParser
from pathlib import Path

from larks.types.m3u import playlist_from_dir


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("root", type=str, nargs="?", default=".")

    return parser.parse_args()


def main():

    args = parse_args()

    root = Path(args.root).resolve()
    if not root.exists():
        raise SystemExit("{} not exists".format(args.root))

    pl = playlist_from_dir(root)

    with os.fdopen(1, "ab") as fp:
        pl.dump(fp)


if __name__ == "__main__":
    main()
