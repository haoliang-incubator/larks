from pathlib import Path
from typing import Optional

import attr


@attr.s
class Album:
    file: Path = attr.ib()
    artist: str = attr.ib()
    name: str = attr.ib()
    suffixes: list[str] = attr.ib()

    discs: list["Disc"] = attr.ib()
    """ discs will be sorted by disc.no """

    year: Optional[int] = attr.ib(default=None)


@attr.s
class Track:
    file: Path = attr.ib()
    no: int = attr.ib()
    name: str = attr.ib()


@attr.s
class Disc:

    file: Path = attr.ib()
    """ when there is only one disc in an album, Disc.file == Album.file """

    no: int = attr.ib()

    tracks: list["Track"] = attr.ib()
    """ tracks will be sorted by track.no """

    name: Optional[str] = attr.ib(default=None)
