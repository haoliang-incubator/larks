"""
directory/file structure:

    Album/Disc/Track
or
    Album/Track

"""

import logging
from pathlib import Path
from typing import Optional

from .types import Album, Disc, Track


def _tracks(root: Path, suffixes: list[str]) -> list[Track]:
    def source():
        for file in root.iterdir():
            # file.suffix starts with dot
            if file.suffix[1:] in suffixes:
                yield track_from_file(file)

    def keyfn(track):
        return track.no

    tracks = list(source())
    tracks.sort(key=keyfn)

    return tracks


def _discs(album: Album) -> list[Disc]:
    def source():
        single = Disc(file=album.file, no=1, tracks=_tracks(album.file, album.suffixes))

        if single.tracks:
            yield single
            return

        for dir in album.file.iterdir():
            if not dir.is_dir():
                continue

            try:
                yield disc_from_dir(dir, album.suffixes)
            except ValueError:
                logging.debug("skipped %s as an album", dir)
                continue

    def keyfn(disc):
        return disc.no

    discs = list(source())
    discs.sort(key=keyfn)

    return discs


def album_from_dir(root: Path) -> Album:
    artist, name, year, suffixes = parse_album_string(root.name.strip())

    album = Album(
        file=root,
        artist=artist,
        name=name,
        year=year,
        suffixes=suffixes,
        discs=[],
    )

    album.discs.extend(_discs(album))

    return album


def disc_from_dir(root: Path, suffixes: list[str]) -> Disc:
    no, name = parse_disc_string(root.name.strip())

    return Disc(file=root, no=no, name=name, tracks=_tracks(root, suffixes))


def track_from_file(file: Path) -> Track:
    no, name = parse_track_string(file.stem.strip())

    return Track(file=file, no=int(no), name=name)


def parse_album_string(string: str):
    """
    form: artist - name (year) [suffix,...]
    """

    rest: str

    artist, rest = map(str.strip, string.split("-", 1))

    if not rest.endswith("]"):
        raise ValueError(f"invalid album due to suffix: {string}")

    suffix_begin = rest.rfind("[")
    suffixes = rest[suffix_begin + 1 : -1].lower().split(",")
    rest = rest[:suffix_begin].strip()

    year: Optional[int]
    if rest.endswith(")"):
        year_begin = rest.rfind("(")
        year = int(rest[year_begin + 1 : -1])
        rest = rest[:year_begin]
    else:
        year = None

    name = rest.strip()

    return artist, name, year, suffixes


def parse_disc_string(string: str):
    """
    forms:
    * `CD`|`Disc` no
    * `Disc` no - name
    """

    rest: str

    kw, rest = string.split(" ", 1)
    rest = rest.lstrip()

    if kw.lower() not in ("cd", "disc"):
        raise ValueError(f"invalid disc name due to keyword, {string}")

    parts = rest.split(" - ")
    nparts = len(parts)

    if nparts == 1:
        no = int(parts[0])
        name = None
    elif nparts == 2:
        no = int(parts[0])
        name = parts[1].strip()
    else:
        raise ValueError(f"invalid disc name due to parts num, {string}")

    return no, name


def parse_track_string(string: str):
    """
    forms:
    * no name
    * no - name
    * no. name
    """

    rest: str

    nolike, rest = string.split(" ", 1)

    if nolike.endswith("."):
        nolike = nolike[:-1]

    try:
        no = int(nolike)
    except TypeError:
        raise ValueError(f"invalid track name due to No, {string}")

    name = rest.lstrip()
    name = name.removeprefix("- ")

    return no, name
