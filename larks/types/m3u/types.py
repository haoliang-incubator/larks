"""
spec: https://en.wikipedia.org/wiki/M3U
"""

from abc import ABC, abstractmethod
from typing import BinaryIO, Iterable, Optional

import attr


class Directive(ABC):
    @abstractmethod
    def __str__(self) -> str:
        ...


@attr.s
class EntryInfo(Directive):
    duration: int = attr.ib()  # in seconds
    title: str = attr.ib()

    def __str__(self) -> str:
        return "#EXTINF:{:d},{:s}".format(self.duration, self.title)


@attr.s
class EntrySize(Directive):
    size: int = attr.ib()  # in bytes

    def __str__(self) -> str:
        return "#EXTBYT:{:d}".format(self.size)


@attr.s
class Entry:

    # possible forms:
    # * absolute local path
    # * relative local path to m3u file
    # * url
    path: str = attr.ib()

    info: Optional[EntryInfo] = attr.ib(default=None)
    size: Optional[EntrySize] = attr.ib(default=None)

    def _lines(self):
        if self.info:
            yield str(self.info)

        if self.size:
            yield str(self.size)

        yield self.path

    def __str__(self):
        return "\n".join(self._lines())


@attr.s
class Playlist:
    artist: str = attr.ib()
    name: str = attr.ib()
    entries: list[Entry] = attr.ib()

    def _lines(self) -> Iterable[str]:

        yield "#EXTM3U"
        yield "#EXTENC:UTF-8"

        for ent in self.entries:
            yield str(ent)

    def __str__(self):
        return "\r\n".join(self._lines())

    def dump(self, fp: BinaryIO):
        for line in self._lines():
            fp.write(line.encode())
            fp.write(b"\r\n")
