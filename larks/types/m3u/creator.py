from pathlib import Path
from typing import Iterable, Optional

from hyperlink import URL
from mutagen import File

from ..file import Album, Track, album_from_dir
from .types import Entry, EntryInfo, Playlist


def _entries(album: Album) -> Iterable[Entry]:

    # TODO@haoliang move hardcode into config
    base = URL.from_text("http://192.168.3.2:8081")

    def url(file: Path) -> str:
        # since parts[0] will always be `/`
        return base.child(*file.parts[1:]).to_text()

    def info(track: Track) -> Optional[EntryInfo]:
        si = File(track.file).info

        try:
            length = round(si.length)
        except AttributeError:
            length = -1  # -1 stands for unknown

        # TODO@haoliang should title contain disc info?
        return EntryInfo(duration=length, title=track.name)

    # we already known album.discs, dics.tracks will be sorted
    for disc in album.discs:
        for track in disc.tracks:
            yield Entry(path=url(track.file), info=info(track))


def playlist_from_dir(root: Path) -> Playlist:
    album = album_from_dir(root)
    ents = list(_entries(album))

    return Playlist(artist=album.artist, name=album.name, entries=ents)
