import logging
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

from larks.types.m3u import playlist_from_dir


class Filesystem:
    albums = [
        Path("/mnt/pearl/music/albums"),
        Path("/mnt/pearl/music/netease-albums"),
    ]
    playlists = Path("/mnt/pearl/music/playlists")


def create_playlist(path: Path):

    try:
        playlist = playlist_from_dir(path)
    except ValueError as e:
        raise SystemExit(repr(e))

    if not playlist.entries:
        logging.info("no tracks in %s", path)
        return

    outname = "{}-{}.m3u8".format(playlist.artist, playlist.name).replace(" ", "-")
    outfile = Filesystem.playlists.joinpath(outname)

    with outfile.open("wb") as fp:
        playlist.dump(fp)
        logging.info("created %s", outname)


def main():

    with ThreadPoolExecutor() as executor:
        for root in Filesystem.albums:
            for path in root.iterdir():
                executor.submit(create_playlist, path)


if __name__ == "__main__":
    logging.basicConfig(
        level="DEBUG",
        style="{",
        datefmt="%Y-%m-%d %H:%M:%S",
        format="{asctime} {message}",
    )

    main()
